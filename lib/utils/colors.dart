import 'package:flutter/material.dart';

const yellowGr = LinearGradient(colors: [Color(0xffff7d04), Color(0xffffcb52)]);
const blackGr = LinearGradient(colors: [Color(0xff545567), Color(0xff2c2c38)]);
const skyblueGr = Color(0xff3effff);
const greenGr = LinearGradient(colors: [Color(0xff2afdb7), Color(0xff08c893)]);
const blueGr = LinearGradient(colors: [Color(0xff1254fc), Color(0xff5581f1)]);
