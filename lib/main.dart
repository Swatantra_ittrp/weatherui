import 'package:flutter/material.dart';
import 'package:learing_flutter/appBar.dart';
import 'package:learing_flutter/cards.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomeScreen(),
    );
  }
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          print("Button Clicked");
        },
        child: Icon(
          Icons.add,
          size: 35,
        ),
        backgroundColor: Color(0xff545567),
      ),
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [Color(0xff545567), Color(0xff2c2c38)])),
          ),
          SingleChildScrollView(
            child: Column(
              children: [
                MyAppBar(),
                MyCard(
                  color1: Color(0xffff7d04),
                  color2: Color(0xffffcb52),
                  country: "NewYork",
                  weatherCondition: "Sunny",
                  temp: "16",
                  
                ),
                MyCard(
                    color1: Color(0xff2afdb7),
                    color2: Color(0xff08c893),
                    country: "Mumbai",
                    weatherCondition: "Sunny",
                    temp: "25"),
                MyCard(
                    color1: Color(0xffc064dd),
                    color2: Color(0xff5d28fe),
                    country: "Sydney",
                    weatherCondition: "Cloudy",
                    temp: "12"),
                MyCard(
                    color1: Color(0xff1254fc),
                    color2: Color(0xff5581f1),
                    country: "NewYork",
                    weatherCondition: "Sunny",
                    temp: "25"),
                MyCard(
                    color1: Color(0xffeecda3),
                    color2: Color(0xffef629f),
                    country: "New Delhi",
                    weatherCondition: "Sunny",
                    temp: "31"),
                MyCard(
                    color1: Color(0xff00416a),
                    color2: Color(0xff799f0c),
                    country: "Venice",
                    weatherCondition: "Cloudy",
                    temp: "18"),
                MyCard(
                    color1: Color(0xffc21500),
                    color2: Color(0xffffc500),
                    country: "Tokyo",
                    weatherCondition: "Rainy",
                    temp: "21"),
              ],
            ),
          )
        ],
      ),
    );
  }
}
