import 'package:flutter/material.dart';

class MyAppBar extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 18, horizontal: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              Text(
                "EDIT",
                style: TextStyle(
                    color: Color(0xff3effff),
                    fontWeight: FontWeight.w600,
                    fontSize: 15),
              ),
              Text(
                "City Management",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                    fontSize: 17),
              ),
              Text(
                "DONE",
                style: TextStyle(
                    color: Color(0xff3effff),
                    fontWeight: FontWeight.w600,
                    fontSize: 15),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
